package config

import (
	"context"
	"fmt"
	"path"
	"reflect"
	"time"

	"github.com/mcuadros/go-defaults"
	"github.com/mitchellh/mapstructure"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"aml/di"
)

const ConfigPath = "./config"

type Config struct {
	Logger
	Output
	Database
	Ethereum
	Cluster
	Graph
}

type Logger struct {
	Level logrus.Level
}

type Output struct {
	ShowSingleAccount  bool   `default:"false"`
	GraphDepositsReuse string `default:"output/cluster.html"`
}

type Ethereum struct {
	Address         string `default:"false"`
	DataDirectory   string `default:"./geth/data/mainnet"`
	BatchBlocksSize uint64 `default:"50"`
	BatchesPoolSize uint64 `default:"1000"`
}

type Database struct {
	Address  string `default:"127.0.0.1:3307"`
	User     string `default:"admin"`
	Password string `default:"admin"`
	Name     string `default:"test"`
	Driver   string `default:"mysql"`
	Clean    bool   `default:"true"`
}

type Graph struct {
	GraphDepth        uint64 `default:"10"`
	NodeMaxCountLinks uint64 `default:"100"`
}

type Cluster struct {
	DepositReuseMaxBlockDiff  uint64 `default:"10000"`
	DepositReuseFeeDiffFactor uint64 `default:"150"`
}

func New() (*Config, error) {
	cfg := new(Config)
	defaults.SetDefaults(cfg)

	cfgViper := viper.New()

	cfgDir, cfgName := path.Split(ConfigPath)

	cfgViper.SetConfigName(cfgName)
	cfgViper.AddConfigPath(cfgDir)

	if err := cfgViper.ReadInConfig(); err != nil {
		return nil, err
	}

	err := cfgViper.Unmarshal(&cfg, viper.DecodeHook(
		mapstructure.ComposeDecodeHookFunc(
			duration,
			logrusLevel,
		),
	))
	if err != nil {
		return nil, fmt.Errorf("failed to parse config: %w", err)
	}

	return cfg, nil
}

func logrusLevel(_ reflect.Type, t reflect.Type, data interface{}) (interface{}, error) {
	if t.String() != "logrus.Level" {
		return data, nil
	}

	return logrus.ParseLevel(data.(string))
}

func duration(_ reflect.Type, t reflect.Type, data interface{}) (interface{}, error) {
	if t.String() != "time.Duration" {
		return data, nil
	}

	return time.ParseDuration(data.(string))
}

func GetConfigFromCtx(ctx context.Context) (cfg Config, err error) {
	err = di.FromContext(ctx).Invoke(func(innerCfg *Config) (innerErr error) {
		cfg = *innerCfg

		return nil
	})

	return
}
