package airdrop

import (
	"context"
	"fmt"

	"aml/core/clustering/common"
	"aml/core/database"
	"aml/di"
)

const (
	minAirdropAccounts = uint64(5)
	blockDiff          = uint64(10000)
)

func Run(ctx context.Context, toBlock uint64) error {
	airdrops, err := getAirdrops(ctx, common.GetFromBlock(toBlock, blockDiff), toBlock, minAirdropAccounts)
	if err != nil {
		return fmt.Errorf("can't get airdrops %w", err)
	}

	fmt.Println("Airdrops:", len(airdrops), "count")

	for _, airdrop := range airdrops {
		err = clustering(ctx, "airdrop", airdrop)
		if err != nil {
			return fmt.Errorf("error clustering: %w", err)
		}
	}

	return nil
}

func clustering(ctx context.Context, heuristic string, airdrop *database.Airdrop) error {
	return di.FromContext(ctx).Invoke(func(db database.Database) error {
		txs, err := db.FindTransfersBetweenMembers(ctx, airdrop)
		if err != nil {
			return err
		}

		return common.Clustering(ctx, heuristic, txs)
	})
}

func getAirdrops(ctx context.Context, fromBlock, toBlock, minSize uint64) (airdrops []*database.Airdrop, err error) {
	err = di.FromContext(ctx).Invoke(func(db database.Database) (innerErr error) {
		airdrops, innerErr = db.GetAirdrops(ctx, fromBlock, toBlock, minSize)

		return innerErr
	})

	return
}
