package selfauth

import (
	"aml/core/clustering/common"
	"aml/core/database"
	"aml/di"
	"context"
	"fmt"
)

const (
	maxApproves = uint64(10)
	blockDiff   = uint64(100000)
)

func Run(ctx context.Context, toBlock uint64) error {
	txs, err := getSelfApproves(ctx, common.GetFromBlock(toBlock, blockDiff), toBlock, maxApproves)
	if err != nil {
		return err
	}

	fmt.Println("Self approves:", len(txs), "count")

	return common.Clustering(ctx, "self auth", txs)
}

func getSelfApproves(ctx context.Context, fromBlock uint64, toBlock uint64, maxApproves uint64) (txs database.Transactions, err error) {
	err = di.FromContext(ctx).Invoke(func(db database.Database) (innerErr error) {
		txs, innerErr = db.GetSelfApproveTxs(ctx, fromBlock, toBlock, maxApproves)

		return innerErr
	})

	return txs, err
}
