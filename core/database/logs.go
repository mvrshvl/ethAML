package database

import (
	logging "aml/log"
	"context"
	"encoding/hex"
	"strings"

	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"

	"aml/test/contract"
)

const ApproveEventTopic = "0x8c5be1e5ebec7d5bd14f71427d1e84f3dd0314c0f7b2291e5b200ac8c7c3b925"

type Log struct {
	LogIndex    string `csv:"log_index"`
	Hash        string `csv:"transaction_hash"`
	Index       string `csv:"transaction_index"`
	BlockHash   string `csv:"block_hash"`
	BlockNumber string `csv:"block_number"`
	Address     string `csv:"Address"`
	Data        string `csv:"data"`
	Topics      string `csv:"topics"`
}

type Logs []*Log

type ERC20Approve struct {
	TxHash          string
	ContractAddress string
	FromAddress     string
	ToAddress       string
}

type ERC20Approves []*ERC20Approve

func (logs Logs) ToApproves(ctx context.Context, txs Transactions) (erc20Approves ERC20Approves, err error) {
	approves := make(map[string]struct{})

	for _, l := range logs {
		if topics := strings.Split(l.Topics, ","); len(l.Topics) != 0 && topics[0] == ApproveEventTopic {
			approves[l.Hash] = struct{}{}
		}
	}

	contractABI, err := abi.JSON(strings.NewReader(contract.ERC20ABI))
	if err != nil {
		return nil, err
	}

	for _, tx := range txs {
		if _, ok := approves[tx.Hash]; ok {
			approve, err := approveFromTx(contractABI, tx)
			if err != nil {
				logging.Debugf(ctx, "can't get approve from tx: %v", err)

				continue
			}

			erc20Approves = append(erc20Approves, approve)
		}
	}

	return
}

func approveFromTx(contractABI abi.ABI, tx *Transaction) (*ERC20Approve, error) {
	decodedData, err := hex.DecodeString(tx.Input[10:])
	if err != nil {
		return nil, err
	}

	inputInterface, err := contractABI.Methods["approve"].Inputs.Unpack(decodedData)
	if err != nil {
		return nil, err
	}

	approve := &ERC20Approve{
		TxHash:          tx.Hash,
		ContractAddress: tx.ToAddress,
		FromAddress:     tx.FromAddress,
	}

	for _, in := range inputInterface {
		switch input := in.(type) {
		case common.Address:
			approve.ToAddress = strings.ToLower(input.String())
		}
	}

	return approve, nil
}
