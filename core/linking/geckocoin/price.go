package gecko

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"aml/amlerror"
)

const (
	baseURL = "https://api.coingecko.com/api/v3"
	badCode = 200

	errBadCode           = amlerror.AMLError("BadCode received")
	errIncorrectResponse = amlerror.AMLError("incorrect response")

	usdCurrency = "usd"
	EthCurrency = "ethereum"

	WeiInEth = 1_000_000_000_000_000_000
)

type Client struct {
	httpClient *http.Client
}

func NewClient() *Client {
	return &Client{httpClient: http.DefaultClient}
}

type TokenPrice map[string]map[string]float64

func (c *Client) GetPrice(currency string) (float64, error) {
	if currency == EthCurrency {
		return c.GetEthPrice()
	}

	return c.GetTokenPrice(currency)
}

func (c *Client) GetTokenPrice(address string) (float64, error) {
	params := url.Values{}

	params.Add("contract_addresses", address)
	params.Add("vs_currencies", usdCurrency)

	resp, err := c.sendGetRequest(fmt.Sprintf("%s/simple/token_price/ethereum?%s", baseURL, params.Encode()))
	if err != nil {
		return 0, err
	}

	return parseResponse(address, usdCurrency, resp)
}

func (c *Client) GetEthPrice() (float64, error) {
	params := url.Values{}

	params.Add("ids", EthCurrency)
	params.Add("vs_currencies", usdCurrency)

	resp, err := c.sendGetRequest(fmt.Sprintf("%s/simple/price?%s", baseURL, params.Encode()))
	if err != nil {
		return 0, err
	}

	return parseResponse(EthCurrency, usdCurrency, resp)
}

func (c *Client) sendGetRequest(query string) ([]byte, error) {
	req, err := http.NewRequest("GET", query, nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if badCode != resp.StatusCode {
		return nil, fmt.Errorf("%w: %s", errBadCode, body)
	}

	return body, nil
}

func parseResponse(id, currency string, resp []byte) (float64, error) {
	var tokenPrice TokenPrice

	err := json.Unmarshal(resp, &tokenPrice)
	if err != nil {
		return 0, err
	}

	for responseAddress, currencies := range tokenPrice {
		if strings.EqualFold(responseAddress, id) {
			return currencies[currency], nil
		}
	}

	return 0, errIncorrectResponse
}
