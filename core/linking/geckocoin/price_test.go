package gecko

import "testing"

const uniswap = "0x1f9840a85d5af5bf1d1762f925bdaddc4201f984"

func TestClient_GetTokenPrice(t *testing.T) {
	client := NewClient()

	price, err := client.GetTokenPrice(uniswap)
	if err != nil {
		t.Fatal(err)
	}

	if price == 0 {
		t.Fatalf("nil received")
	}
}

func TestClient_GetEthPrice(t *testing.T) {
	client := NewClient()

	price, err := client.GetEthPrice()
	if err != nil {
		t.Fatal(err)
	}

	if price == 0 {
		t.Fatalf("nil received")
	}
}
