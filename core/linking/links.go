package linking

import (
	"context"

	"github.com/google/uuid"

	"aml/amlerror"
	"aml/core/database"
	"aml/core/linking/node"
	"aml/core/linking/prices"
	"aml/di"
)

const (
	errCurrency = amlerror.AMLError("currency not found")
)

type Links struct {
	ID          string
	links       node.Node
	Prices      *prices.Prices
	Nodes       *node.NodesStorage
	MainAddress string
}

func Run(ctx context.Context, address string) (*Links, error) {
	storage := node.NewCache()

	root, err := node.New(ctx, address, storage)
	if err != nil {
		return nil, err
	}

	err = root.Link(ctx, 0)
	if err != nil {
		return nil, err
	}

	return new(ctx, root, storage, address), nil
}

func new(ctx context.Context, node node.Node, cache *node.NodesStorage, mainAddress string) *Links {
	links := &Links{
		ID:          uuid.New().String(),
		links:       node,
		Nodes:       cache,
		MainAddress: mainAddress,
	}

	links.Prices = prices.New(ctx, links.GetCurrencies())

	return links
}

func (l *Links) GetMainNode() *node.SingleNode {
	return l.links.GetNode(l.MainAddress)
}

func (l *Links) GetMainAddress() string {
	return l.MainAddress
}

func (l *Links) GetNodes() *node.NodesStorage {
	return l.Nodes
}

func (l *Links) GetID() string {
	return l.ID
}

func (l *Links) GetRoot() node.Node {
	return l.links
}

func (l *Links) CalculateRisks(ctx context.Context) error {
	risksByTypes, err := getAccountTypesRisks(ctx)
	if err != nil {
		return err
	}

	l.links.CalculateRisk(nil, l.Prices, risksByTypes)

	return nil
}

func (l *Links) GetCurrencies() map[string]struct{} {
	currencies := make(map[string]struct{})
	for _, link := range l.Nodes.GetAll() {
		for _, tx := range link.GetTxs() {
			currencies[prices.GetCurrency(tx)] = struct{}{}
		}
	}

	return currencies
}

func (l *Links) GetByCurrency(ctx context.Context, currency string) (*Links, error) {
	currencies := l.GetCurrencies()
	if _, ok := currencies[currency]; !ok {
		return nil, errCurrency
	}

	risksByTypes, err := getAccountTypesRisks(ctx)
	if err != nil {
		return nil, err
	}

	storage := node.NewCache()

	linksByCurrency := l.links.CopyByCurrency(storage, currency)
	linksByCurrency.CalculateRisk(nil, l.Prices, risksByTypes)

	return &Links{
		ID:          l.ID,
		links:       linksByCurrency,
		Prices:      l.Prices,
		Nodes:       storage,
		MainAddress: l.MainAddress,
	}, nil
}

func getAccountTypesRisks(ctx context.Context) (risks map[database.AccountType]int, err error) {
	err = di.FromContext(ctx).Invoke(func(db database.Database) (innerErr error) {
		risks, innerErr = db.GetAccountTypesRisks(ctx)

		return innerErr
	})

	return
}

func (l *Links) GetPrices() *prices.Prices {
	return l.Prices
}
