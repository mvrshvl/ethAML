package server

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"

	"aml/config"
	"aml/core/linking"
	"aml/core/linking/graph"
	"aml/core/server/storage"
	logging "aml/log"
)

const (
	index = "<!DOCTYPE html>\n<html lang=\"ru\">\n<head>\n    <meta charset=\"UTF-8\">\n    <title>AML</title>\n    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js\"></script>\n    <script src=\"/telegramform.js\"></script>\n    <style>\n        body{\n            background-color: #242424;\n        }\n        .button {\n            width: 100%; display: inline-block;\n            border-radius: 4px;\n            background-color: #f0f8ff;\n            border: none;\n            color: #242424;\n            text-align: center;\n            font-size: 28px;\n            padding: 20px;\n          transition: all 0.5s;\n       margin: 5px 0;     cursor: pointer;\n        }\n\n        .button span {\n            cursor: pointer;\n            display: inline-block;\n            position: relative;\n            transition: 0.5s;\n        }\n\n        .button span:after {\n            content: '\\00bb';\n            position: absolute;\n            opacity: 0;\n            top: 0;\n            right: -20px;\n            transition: 0.5s;\n        }\n\n        .button:hover span {\n            padding-right: 25px;\n        }\n\n        .button:hover span:after {\n            opacity: 1;\n            right: 0;\n        }\n        .token{\n            position: center;\n       }\n     .tokenform {    width: 50vw;\n    margin: 40vh auto; }  .tokenform div { max-height: 20vh; } .tokeninp{\n            position: center;\n            width: -webkit-fill-available;\n        }\n    </style>\n    <script>\n\n    </script>\n</head>\n<body>\n<div class=\"token\">\n    <form class=\"tokenform\" method=\"GET\" action=\"http://localhost:8080/analyze\">\n        <div>\n            <input class=\"tokeninp\" type=\"text\" placeholder=\"Enter ethereum address\" name=\"address\">\n            <button class=\"button\" style=\"vertical-align:middle\" type=\"submit\"><span>Analyze address</span></button>\n        </div>\n    </form>\n</div>\n</body>\n</html>" //nolint:lll
)

type Controller struct{}

func NewController() *Controller {
	return &Controller{}
}

func (c *Controller) GroupWithCtx(ctx context.Context) func(router *gin.Engine) {
	return func(router *gin.Engine) {
		group := router.Group("/")
		{
			group.GET("/", func(c *gin.Context) {
				c.Data(http.StatusOK, "text/html; charset=utf-8", []byte(index))
			})
			group.GET("/analyze", analyze(ctx))
			group.GET("/report/:id", report(ctx))
			group.GET("/report/:id/graph", reportGraph(ctx))
			group.GET("/report/:id/raw", reportRaw(ctx))
			group.GET("/docs", func(c *gin.Context) {
				c.Data(http.StatusOK, "text/html; charset=utf-8", []byte(docPage))
			})
		}
	}
}

func analyze(ctx context.Context) func(c *gin.Context) {
	return func(c *gin.Context) {
		mainAddress := strings.ToLower(c.Query("address"))

		currentBlock, err := getCurrentBlock(ctx)
		if err != nil {
			ginErr(c, fmt.Errorf("can't get last block: %w", err))
			return
		}

		currentTime := time.Now()

		fmt.Println("START LINKING")
		links, err := linking.Run(ctx, mainAddress)
		if err != nil {
			ginErr(c, fmt.Errorf("can't linking account: %w", err))
			return
		}

		fmt.Println("START CALCULATE", len(links.GetNodes().GetAll()))
		err = links.CalculateRisks(ctx)
		if err != nil {
			ginErr(c, err)
			return
		}

		report := &storage.Report{Links: links, Report: []byte(newReport(links, currentBlock, currentTime))}

		err = storage.AddWithCtx(ctx, report)
		if err != nil {
			ginErr(c, err)
			return
		}

		c.Data(http.StatusOK, "text/html; charset=utf-8", report.Report)
	}
}

func report(ctx context.Context) func(c *gin.Context) {
	return func(c *gin.Context) {
		report, err := storage.GetWithCtx(ctx, c.Param("id"))
		if err != nil {
			ginErr(c, err)
			return
		}

		c.Data(http.StatusOK, "text/html; charset=utf-8", report.Report)
	}
}

func reportRaw(ctx context.Context) func(c *gin.Context) {
	return func(c *gin.Context) {
		report, err := storage.GetWithCtx(ctx, c.Param("id"))
		if err != nil {
			ginErr(c, err)
			return
		}

		rawReport, err := json.Marshal(report.Links)
		if err != nil {
			ginErr(c, err)
			return
		}

		c.Data(http.StatusOK, "text/html; charset=utf-8", rawReport)
	}
}

func reportGraph(ctx context.Context) func(c *gin.Context) {
	return func(c *gin.Context) {
		report, err := storage.GetWithCtx(ctx, c.Param("id"))
		if err != nil {
			ginErr(c, err)
			return
		}

		links := report.Links

		currency := c.Query("currency")
		if len(currency) != 0 {
			links, err = report.Links.GetByCurrency(ctx, currency)
			if err != nil {
				ginErr(c, err)
				return
			}
		}

		cfg, err := config.GetConfigFromCtx(ctx)
		if err != nil {
			ginErr(c, err)
			return
		}

		b, err := graph.Run(links, links.GetMainAddress(), int(cfg.Graph.GraphDepth), int(cfg.Graph.NodeMaxCountLinks))
		if err != nil {
			logging.Error(ctx, "can't generate graph:", err)
			return
		}

		c.Data(http.StatusOK, "text/html; charset=utf-8", b.Bytes())
	}
}

func ginErr(ctx *gin.Context, err error) {
	ctx.Data(http.StatusBadRequest, "text/html; charset=utf-8", []byte(err.Error()))
}
