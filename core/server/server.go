package server

import (
	"context"

	"github.com/gin-gonic/gin"
)

type Server struct {
	Router *gin.Engine

	addr string
}

func New(addr string) *Server {
	return &Server{
		addr: addr,
	}
}

func (s *Server) Run(ctx context.Context) error {
	s.Router = gin.Default()

	clusterController := NewController()
	clusterController.GroupWithCtx(ctx)(s.Router)

	err := s.Router.Run(s.addr)
	if err != nil {
		return err
	}

	return nil
}
