package geth

import (
	"context"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path"
	"strconv"
	"time"

	"github.com/gocarina/gocsv"

	"aml/config"
	"aml/core/database"
	"aml/di"
	"aml/log"
)

func LoadBlocks(ctx context.Context, batchesPoolSize uint64, errNotify chan error) (chan *database.NewBlocks, error) {
	notifyBlock := make(chan *database.NewBlocks, batchesPoolSize)

	err := di.FromContext(ctx).Invoke(func(db database.Database, cfg *config.Config) error {
		innerErr := db.Connect(ctx)
		if innerErr != nil {
			return innerErr
		}

		innerErr = addExchanges(ctx, db, cfg.DataDirectory)
		if innerErr != nil {
			return innerErr
		}

		innerErr = addTypedAccounts(ctx, db, cfg.DataDirectory)
		if innerErr != nil {
			return innerErr
		}

		dbBlockNum, innerErr := db.GetLastBlock(ctx)
		if innerErr != nil {
			return innerErr
		}

		go collectData(ctx, cfg.DataDirectory, cfg.BatchBlocksSize, dbBlockNum, notifyBlock, errNotify)

		return nil
	})

	return notifyBlock, err
}

func addExchanges(ctx context.Context, db database.Database, directory string) error {
	var exchanges database.Exchanges

	err := ParseCSV(path.Join(directory, "exchanges.csv"), &exchanges)
	if err != nil {
		return fmt.Errorf("can't parse exchanges: %w", err)
	}

	return exchanges.AddExchanges(ctx, db.GetConnection())
}

func collectData(ctx context.Context, directory string, batchBlocksSize uint64, fromBlock uint64, notifyChan chan *database.NewBlocks, errNotify chan error) {
	return
	var (
		ethBlock uint64
		blocks   *database.NewBlocks
		err      error
	)

	defer func() {
		if err != nil {
			errNotify <- err
		}
	}()

	ticker := time.NewTicker(time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
		}

		err = di.FromContext(ctx).Invoke(func(w *Worker) (innerErr error) {
			ethBlock, innerErr = w.GetBlock(ctx, int64(fromBlock+batchBlocksSize))
			return
		})
		if err != nil {
			log.Warn(ctx, "Eth getBlock err", err, fromBlock+batchBlocksSize)
			continue
		}

		blocks, err = downloadData(ctx, directory, fromBlock+1, ethBlock)
		if err != nil {
			log.Errorf(ctx, "can't download eth data: %v", err)
			continue
		}

		if len(blocks.Blocks) == 0 {
			log.Warn(ctx, "block blocks err")

			continue
		}

		err = blocks.Save(ctx)
		if err != nil {
			log.Errorf(ctx, "can't save eth data: %v", err)

			return
		}

		select {
		case notifyChan <- blocks:
		default:
			log.Warn(ctx, "can't send newBlocks to clustering")

			continue
		}

		fromBlock += batchBlocksSize
	}
}

func downloadData(ctx context.Context, directory string, fromBlock, toBlock uint64) (*database.NewBlocks, error) {
	err := di.FromContext(ctx).Invoke(func(cfg *config.Config) error {
		cmd := exec.Command("bash", "./geth/download.sh", strconv.FormatUint(fromBlock, 10), strconv.FormatUint(toBlock, 10), cfg.Ethereum.Address, cfg.DataDirectory)

		//cmd.Stdout = log.GetStdout(ctx)
		//cmd.Stderr = cmd.Stdout

		return cmd.Run()
	})
	if err != nil {
		return nil, err
	}

	return parseNewBlocks(ctx, directory)
}

func parseNewBlocks(ctx context.Context, directory string) (*database.NewBlocks, error) {
	var (
		blocks         []*database.Block
		txs            []*database.Transaction
		exchanges      []*database.Exchange
		tokenTransfers database.TokenTransfers
		logs           database.Logs
	)

	err := ParseCSV(path.Join(directory, "blocks.csv"), &blocks)
	if err != nil {
		return nil, err
	}

	err = ParseCSV(path.Join(directory, "transactions.csv"), &txs)
	if err != nil {
		return nil, err
	}

	err = ParseCSV(path.Join(directory, "logs.csv"), &logs)
	if err != nil {
		return nil, err
	}

	err = ParseCSV(path.Join(directory, "token_transfers.csv"), &tokenTransfers)
	if err != nil {
		return nil, err
	}

	return database.GetNewBlocks(ctx, txs, blocks, exchanges, tokenTransfers, logs)
}

func ParseCSV(filename string, out interface{}) error {
	f, err := os.OpenFile(filename, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return err
	}

	err = gocsv.UnmarshalCSV(gocsv.DefaultCSVReader(f), out)
	if err != nil && !errors.Is(err, gocsv.ErrEmptyCSVFile) {
		return err
	}

	return nil
}
